package com.example.parkingviolationsonblast

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Application to display open parking and camera violations.
 *
 * @constructor Create empty Violation app
 */
@HiltAndroidApp
class ViolationApp : Application()
