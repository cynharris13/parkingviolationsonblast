package com.example.parkingviolationsonblast.model.dto

@kotlinx.serialization.Serializable
data class SummonsImage(
    val description: String = "",
    val url: String = ""
)
