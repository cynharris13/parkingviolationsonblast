package com.example.parkingviolationsonblast.model

import com.example.parkingviolationsonblast.model.dto.ViolationResponseListItem
import com.example.parkingviolationsonblast.model.remote.ViolationService
import javax.inject.Inject

/**
 * Violation repository.
 *
 * @property service
 * @constructor Create empty Violation repository
 */
class ViolationRepository @Inject constructor(private val service: ViolationService) {
    /**
     * Get all violations.
     *
     * @return the list of violation objects.
     */
    suspend fun getAllViolations(): List<ViolationResponseListItem> {
        return service.getAllViolations().body() ?: emptyList()
    }
}
