package com.example.parkingviolationsonblast.model.dto

import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
data class ViolationResponseListItem(
    @SerialName("amount_due") val amountDue: String = "",
    val county: String = "",
    @SerialName("fine_amount") val fineAmount: String = "",
    @SerialName("interest_amount") val interestAmount: String = "",
    @SerialName("issue_date") val issueDate: String = "",
    @SerialName("issuing_agency") val issuingAgency: String = "",
    @SerialName("judgment_entry_date") val judgementEntryDate: String = "",
    @SerialName("license_type") val licenseType: String = "",
    @SerialName("payment_amount") val paymentAmount: String = "",
    @SerialName("penalty_amount") val penaltyAmount: String = "",
    val plate: String = "",
    val precinct: String = "",
    @SerialName("reduction_amount") val reductionAmount: String = "",
    val state: String = "",
    @SerialName("summons_image") val summonsImage: SummonsImage = SummonsImage(),
    @SerialName("summons_number") val summonsNumber: String = "",
    val violation: String = "",
    @SerialName("violation_status") val violationStatus: String = "",
    @SerialName("violation_time") val violationTime: String = ""
)
