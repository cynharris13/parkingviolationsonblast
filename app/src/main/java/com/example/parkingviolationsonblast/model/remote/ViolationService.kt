package com.example.parkingviolationsonblast.model.remote

import com.example.parkingviolationsonblast.model.dto.ViolationResponseListItem
import retrofit2.Response
import retrofit2.http.GET

/**
 * Violation service.
 *
 * @constructor Create empty Violation service
 */
interface ViolationService {
    @GET(VIOLATION_ENDPOINT)
    suspend fun getAllViolations(): Response<ArrayList<ViolationResponseListItem>>

    companion object {
        const val VIOLATION_ENDPOINT = "nc67-uf89.json"
    }
}
