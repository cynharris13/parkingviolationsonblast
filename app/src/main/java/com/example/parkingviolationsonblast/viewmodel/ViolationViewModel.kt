package com.example.parkingviolationsonblast.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.parkingviolationsonblast.model.ViolationRepository
import com.example.parkingviolationsonblast.util.Attribute
import com.example.parkingviolationsonblast.view.uistate.ViolationUIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Violation view model.
 *
 * @property repository
 * @constructor Create empty Violation view model
 */
@HiltViewModel
class ViolationViewModel @Inject constructor(private val repository: ViolationRepository) : ViewModel() {
    private val _state = MutableStateFlow(ViolationUIState())
    val state: StateFlow<ViolationUIState> get() = _state

    init { getAllViolations() }

    private fun getAllViolations() = viewModelScope.launch {
        _state.update { it.copy(isLoading = true) }
        val violations = repository.getAllViolations()
        _state.update { it.copy(isLoading = false, violationList = violations, filteredList = violations) }
    }

    /**
     * Filter violation list.
     *
     * @param attribute
     * @param match
     */
    fun filterViolationList(attribute: Attribute, match: String) = viewModelScope.launch {
        if (match.isEmpty()) { _state.update { it.copy(filteredList = it.violationList) } } else {
            val newViolationList = state.value.violationList?.filter { attribute.match(match, it) }
            _state.update { it.copy(filteredList = newViolationList) }
        }
    }
}
