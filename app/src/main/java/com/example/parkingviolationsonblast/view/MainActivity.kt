@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.parkingviolationsonblast.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.FilterList
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.parkingviolationsonblast.model.dto.ViolationResponseListItem
import com.example.parkingviolationsonblast.ui.theme.ParkingViolationsOnBlastTheme
import com.example.parkingviolationsonblast.util.Attribute
import com.example.parkingviolationsonblast.viewmodel.ViolationViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlin.random.Random

const val SEARCH_TEXT_WEIGHT = 4f
const val MAX_COLOR_VALUE = 255
const val LOWER_BOUNDS = 64
const val UPPER_BOUNDS = 193

var entryColor = Color.Black

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private val violationViewModel by viewModels<ViolationViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val state by violationViewModel.state.collectAsState()
            ParkingViolationsOnBlastTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column(modifier = Modifier.fillMaxSize()) {
                        Filter { attribute, input ->
                            violationViewModel.filterViolationList(attribute, input)
                        }
                        state.filteredList?.let {
                            DisplayViolations(it)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun Filter(onClick: (Attribute, String) -> Unit) {
    Row(modifier = Modifier.fillMaxWidth()) {
        var expanded by remember { mutableStateOf(false) }
        var selectedAttribute by remember { mutableStateOf(Attribute.Violation) }
        IconButton(onClick = { expanded = true }, modifier = Modifier.weight(1f)) {
            Icon(
                imageVector = Icons.TwoTone.FilterList,
                contentDescription = null
            )
            DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
                Attribute.values().forEach { s ->
                    DropdownMenuItem(
                        text = {
                            val check = if (s == selectedAttribute) "✔️" else ""
                            Text(text = "${s.type} $check")
                        },
                        onClick = {
                            selectedAttribute = s
                            expanded = false
                        }
                    )
                }
            }
        }
        var input by remember { mutableStateOf("") }
        TextField(
            value = input,
            onValueChange = {
                input = it
                onClick(selectedAttribute, input)
            },
            modifier = Modifier.weight(SEARCH_TEXT_WEIGHT),
            colors = TextFieldDefaults.textFieldColors(containerColor = MaterialTheme.colorScheme.surface)
        )
    }
}

@Composable
fun DisplayViolations(violationsList: List<ViolationResponseListItem>) {
    LazyColumn {
        items(violationsList) {
            entryColor = getRandomColor()
            Card(
                elevation = CardDefaults.cardElevation(defaultElevation = 10.dp),
                modifier = Modifier.padding(16.dp)
            ) {
                Column(modifier = Modifier.fillMaxWidth().background(entryColor).padding(16.dp)) {
                    MakeText("${Attribute.Violation.type}: ${it.violation}")
                    MakeText("${Attribute.FineAmount.type}: ${it.fineAmount}")
                    MakeText("${Attribute.Interest.type}: ${it.interestAmount}")
                    MakeText("${Attribute.Paid.type}: ${it.paymentAmount}")
                    MakeText("${Attribute.Due.type}: ${it.amountDue}")
                    MakeText("${Attribute.County.type}: ${it.county}")
                    MakeText("${Attribute.IssueDate.type}: ${it.issueDate}")
                    MakeText("${Attribute.Agency.type}: ${it.issuingAgency}")
                    MakeText("${Attribute.JudgementDate.type}: ${it.judgementEntryDate}")
                    MakeText("${Attribute.LicenseType.type}: ${it.licenseType}")
                    MakeText("${Attribute.Plate.type}: ${it.plate}")
                    MakeText("${Attribute.Precinct.type}: ${it.precinct}")
                    MakeText("${Attribute.Reduction.type}: ${it.reductionAmount}")
                    MakeText("${Attribute.State.type}: ${it.state}")
                    MakeText("${Attribute.Status.type}: ${it.violationStatus}")
                    MakeText("${Attribute.Time.type}: ${it.violationTime}")
                    MakeText("${Attribute.Summons.type}: ${it.summonsNumber}")
                }
            }
            Divider(
                modifier = Modifier.padding(horizontal = 12.dp, vertical = 12.dp),
                color = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.08f)
            )
        }
    }
}

@Composable
fun MakeText(text: String) = Text(text = text, color = complimentaryOf(entryColor))

/**
 * Complimentary color of [entryColor].
 *
 * @param entryColor
 * @return
 */
fun complimentaryOf(entryColor: Color): Color = Color(
    red = 1 - entryColor.red,
    green = 1 - entryColor.green,
    blue = 1 - entryColor.blue
)

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ParkingViolationsOnBlastTheme {
        DisplayViolations(listOf(ViolationResponseListItem(amountDue = "$100")))
    }
}

/**
 * Get random color.
 *
 * @return
 */
fun getRandomColor(): Color {
    val red = randomValue()
    val blue = randomValue()
    val green = randomValue()
    return if (red.isMiddling() || blue.isMiddling() || green.isMiddling()) {
        getRandomColor()
    } else { Color(red = red, blue = blue, green = green) }
}

/**
 * Is middling color value.
 *
 * @return
 */
fun Int.isMiddling(): Boolean = this in LOWER_BOUNDS until UPPER_BOUNDS

/**
 * Random value between 0 and 255, used to generate colors.
 *
 */
fun randomValue() = Random.nextInt(0, MAX_COLOR_VALUE)
