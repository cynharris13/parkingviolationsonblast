package com.example.parkingviolationsonblast.view.uistate

import com.example.parkingviolationsonblast.model.dto.ViolationResponseListItem

/**
 * Violation UI state.
 *
 * @property isLoading
 * @property violationList
 * @property filteredList
 * @constructor Create empty Violation u i state
 */
data class ViolationUIState(
    val isLoading: Boolean = false,
    val violationList: List<ViolationResponseListItem>? = null,
    val filteredList: List<ViolationResponseListItem>? = null
)
