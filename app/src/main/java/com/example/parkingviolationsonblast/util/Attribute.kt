package com.example.parkingviolationsonblast.util

import com.example.parkingviolationsonblast.model.dto.ViolationResponseListItem

/**
 * Attribute.
 *
 * @property type
 * @constructor Create empty Attribute
 */
enum class Attribute(val type: String) {
    Violation("Violation") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean {
            return filter.uppercase() in item.violation.uppercase()
        }
    },
    FineAmount("Fine amount") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.fineAmount == filter
    },
    Interest("Interest") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.interestAmount == filter
    },
    Paid("Amount paid") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.paymentAmount == filter
    },
    Due("Amount due") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.amountDue == filter
    },
    County("County") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.county == filter
    },
    IssueDate("Issue Date") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.issueDate == filter
    },
    Agency("Issuing Agency") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean {
            return filter.uppercase() in item.issuingAgency.uppercase()
        }
    },
    JudgementDate("Entry date of judgment") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.judgementEntryDate == filter
    },
    LicenseType("License type") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.licenseType == filter
    },
    Plate("License plate") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean {
            return filter.uppercase() in item.plate.uppercase()
        }
    },
    Precinct("Precinct") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.precinct == filter
    },
    Reduction("Reduction amount") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.reductionAmount == filter
    },
    State("State of origin") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.state == filter
    },
    Status("Status") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean {
            return filter.uppercase() in item.violationStatus
        }
    },
    Time("Time of violation") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean {
            return filter.uppercase() in item.violationTime
        }
    },
    Summons("Summons number") {
        override fun match(filter: String, item: ViolationResponseListItem): Boolean = item.summonsNumber == filter
    };

    /**
     * Matches [filter] to relevant [item] attribute.
     *
     * @param filter
     * @param item
     * @return
     */
    abstract fun match(filter: String, item: ViolationResponseListItem): Boolean
}
